#include <stdint.h>
#include <stdbool.h>

#include <DataStructures.h>
#include <Utilities.h>
#include <AsyncTelegram.h>


#include "time.h"
#include "WiFi.h"
#include <WebOTA.h>

#include "protocol.h"
#include "secrets.h"

typedef struct {
  uint8_t red;
  uint8_t green;
  uint8_t blue;
} color;

const color colors[] {
  {15, 0, 0}, // red
  {0, 15, 0}, // green
  {0, 0, 15}, // blue
  {5, 0, 7},  // purple
  {15, 8, 0} // orange
};

typedef struct {
  uint8_t index;
  char*   value;
} letter;

const letter letters[] {
  {0, "A"},
  {1, "B"},
  {2, "C"},
  {3, "D"},
  {4, "E"},
  {5, "F"},
  {6, "G"},
  {7, "H"},
  {8, "I"},
  {9, "J"},
  {10, "K"},
  {11, "L"},
  {12, "M"},
  {13, "N"},
  {14, "O"},
  {15, "P"},
  {16, "Q"},
  {17, "R"},
  {18, "S"},
  {19, "T"},
  {20, "U"},
  {21, "V"},
  {22, "W"},
  {23, "X"},
  {24, "Y"},
  {25, "Z"}
};

const int colors_size = sizeof(colors)/sizeof(color);
const int letters_size = sizeof(letters)/sizeof(letter);

void printLocalTime()
{
  struct tm timeinfo;
  if(!getLocalTime(&timeinfo)){
    Serial.println("Failed to obtain time");
    return;
  }
  Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S");
}

AsyncTelegram myBot;

void
setup() {
  randomSeed(analogRead(0));
  
  Serial.begin(115200);
  
  protocol_init();

  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  WiFi.config(INADDR_NONE, INADDR_NONE, INADDR_NONE);
  WiFi.setHostname(WIFI_HOSTNAME);
  delay(100);
  WiFi.begin(SECRET_WIFI_SSID, SECRET_WIFI_PASSWORD);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println(" WIFI CONNECTED");
  
  configTime(0, 0, NTP_SERVER);
  printLocalTime();
  
  // Setup the Telegram bot
  myBot.setUpdateTime(2000);
  myBot.setTelegramToken(TELEGRAM_TOKEN);
    
  // Check if all things are ok
  Serial.print("\nTest Telegram connection... ");
  myBot.begin() ? Serial.println("OK") : Serial.println("NOK");
    
  Serial.print("Bot name: @");  
  Serial.println(myBot.userName);

  webota.init(HTTP_PORT, "/update");
}

void
loop() {
  //Serial.println("starting loop");

  // make sure wifi is up
  // _should_ reconnect on its own _if_ the wifi code runs
  if (WiFi.status() != WL_CONNECTED) {
    delay(250);
    return;
  }

  // clear
  protocol_set_led_state(63, 0, 0, 0, 0 );

  // a variable to store telegram message data
  TBMessage msg;
  if (myBot.getNewMessage(msg)){
    Serial.print("New Message: ");
    Serial.println(msg.text);
    
    String message = msg.text;
    message.toUpperCase();
    //Serial.println(message);
    //Serial.print("length: ");
    //Serial.println(message.length());
    
    int rand_idx;
    for ( int i = 0; i < message.length(); i++ ) {
      char input_letter = message[i];
      if ( i == 0 && input_letter == "/"[0] ) {
        Serial.println("leading slash, skipping message");
        break;
      }
      //Serial.print(input_letter);
      // find our letter in the array of letters
      // my _kingdom_ for a functioning hashmap...
      int ltr_idx = -1;
      for ( int j = 0; j < letters_size; j++ ) {
        if ( *letters[j].value == input_letter ) {
          ltr_idx = letters[j].index;
          break;
        }
      }
      //Serial.print(":  ");   
      //Serial.println(ltr_idx);
      if ( ltr_idx == -1 ) {
        protocol_set_led_state(ltr_idx, 0, 0, 0, 0);
      }
      else {
        // pick a random color for the bulb
        rand_idx = random(0, colors_size);
        //Serial.print("color index: ");
        //Serial.println(rand_idx);
        protocol_set_led_state(
          ltr_idx,
          colors[rand_idx].red,
          colors[rand_idx].green,
          colors[rand_idx].blue,
          100
        );
      }
      delay(1000 + random(0, 500));
      protocol_set_led_state(ltr_idx, 0, 0, 0, 0);
    }
  }
  else {
    delay(500 + random(0, 250));
  }

  // check for updates
  webota.handle();
      
  //Serial.println("loop done");
}
