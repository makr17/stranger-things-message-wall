// protocol.cpp - The GE Color Effects Serial protoc5ol
//
// Copyright (c) 2011 John Graham-Cumming
//
// Provides functions for initializing the display and sending color
// and brightness information to LEDs.
//
// This file is based on the work done by Robert Sun Quattlebaum and
// published on his web site.  The URL for the entry of GE hacking is:
// http://www.deepdarc.com/2010/11/27/hacking-christmas-lights/

#include "protocol.h"


// The Arduino Pro pin to which the display is connected

#define DISPLAY_PIN 2

// NOTE: Macros are used instead of functions so that we have
// maximum control of timing of the serial protocol

// send_one, send_zero: send a one or zero bit to the LEDs with
// correct timing
  
// The timing for a bit one is as follows:
//
// The line is driven low for 20us then held high
// for 10us.
//
// Note that the actual values used here were determined
// by experimentation and by measurement using the logic
// analyzer.
#define send_one                     \ 
{                                    \
  GPIO.out_w1tc = ((uint32_t)1 << DISPLAY_PIN); \
  delayMicroseconds( 18 );           \
  GPIO.out_w1ts = ((uint32_t)1 << DISPLAY_PIN); \
  delayMicroseconds( 8 );            \
  GPIO.out_w1tc = ((uint32_t)1 << DISPLAY_PIN); \
}

// The timing for a bit zero is as follows:
//
// The line is driven low for 20us then held high
// for 10us.
//
// Note that the actual values used here were determined
// by experimentation and by measurement using the logic
// analyzer.
#define send_zero                    \
{                                    \
  GPIO.out_w1tc = ((uint32_t)1 << DISPLAY_PIN); \
  delayMicroseconds( 8 );            \
  GPIO.out_w1ts = ((uint32_t)1 << DISPLAY_PIN); \
  delayMicroseconds( 20 );           \
  GPIO.out_w1tc = ((uint32_t)1 << DISPLAY_PIN); \
}

// send_bits: transmits up to 8 bits to the LED sending from the MSB
// first
#define send_bits( _v, _b )            \ 
{                                      \
  int _c = _b;                         \
  while ( _c > 0 ) {                   \
    if ( _v & ( 1 << ( _c - 1 ) ) ) {  \
      send_one                         \
    } else {                           \
      send_zero                        \
    }                                  \
    _c -= 1;                           \
  }                                    \
}

// start_packet: set the line for sending a packet
// Each packet begins with 10us of bus high
#define start_packet                  \
{                                     \
  GPIO.out_w1ts = ((uint32_t)1 << DISPLAY_PIN); \
  delayMicroseconds( 10 );            \
  GPIO.out_w1tc = ((uint32_t)1 << DISPLAY_PIN); \
}

// end_packet: called when the packet has been transmitted
// Must ensure that the bus is idle for at least 30us
// between packets
#define end_packet                       \
{                                        \
  GPIO.out_w1tc = ((uint32_t)1 << DISPLAY_PIN); \
  delayMicroseconds( 30 );               \
}

// protocol_set_led_state: sets the state (color and brightness) of an
// LED in the display.  The (x, y) coordinates range from (0,0)
// to (6,6).  The color is (r, g, b) where each value is between
// 0 and 15.  The brightness is between 0 and 255, but notes in the
// blog post mentioned above say that 0xCC seems to be the maximum
// so I make an explicit check for that here to ensure that no
// LEDs are damaged
void protocol_set_led_state( int id, // led to set
                             byte r, byte g, byte b, // Color 
                             byte i ) // Brightness
{
  
  // Ensure that brightness cannot go too high
  
  if ( i > MAX_BRIGHTNESS ) {
    i = MAX_BRIGHTNESS;
  }
  
  // The packet format is as follows:
  // 
  // Name             Bits
  // ----             ----
  //
  // Start bit           1
  // Bulb address        6
  // Brightness          8
  // Blue                4
  // Green               4
  // Red                 4
  //
  // Note that values are sent MSB first

  portDISABLE_INTERRUPTS();
  start_packet
  send_bits( id, 6 )
  send_bits( i, 8 )
  send_bits( b, 4 )
  send_bits( g, 4 )
  send_bits( r, 4 )
  end_packet
  portENABLE_INTERRUPTS();
}

// init_led: send the ID to the next LED in the chain
void init_led( int id ) {  // Y-coordinate of LED
  //Serial.print("init ");
  //Serial.println(id);
  // Initializing the LED is actually just achieved by sending it a packet setting
  // its ID.  This has to be done at power up and is called from protocol_init

  protocol_set_led_state( id, 4, 5, 6, 100 );
  
  // Observing the start up of the actual GE lights shows that it allows 0.005s
  // between initializations.  I don't know if this is necessary, but I am going
  // to follow the same
  
  delay( 50 );
}

// protocol_init: Set up a newly powered-on string of lights.
// scan through lights from 0 to 49
void protocol_init()
{
  portDISABLE_INTERRUPTS();
  pinMode( DISPLAY_PIN, OUTPUT );
  GPIO.out_w1tc = ((uint32_t)1 << DISPLAY_PIN);
  portENABLE_INTERRUPTS();
  
  // This is done so we see that the bus is low before we do anything on it
  delay( 1000 );
    
  portDISABLE_INTERRUPTS();
  for ( int id = 0; id < 50; id++ ) {
    init_led( id );
  }
  portENABLE_INTERRUPTS();
}

// protocol_broadcast: helper function that sets color and brightness
// on every LED using the broadcast functionality (ID set to 63 which 
// corresponds to (7, 7)
void protocol_broadcast( int r, int g, int b, // Color
                         int i )              // Brightness
{
    protocol_set_led_state( 63, r, g, b, i );
}

// protocol_test_card: run through test images to show that the display is working
void protocol_test_card()
{
  protocol_broadcast( 0, 0, 0, 0 );
  
  // 1. Set all the LEDs to white and fade from 0 to maximum

  for ( int i = 0; i < MAX_BRIGHTNESS; i += 10 ) {
      
    // Note that 13 is used here for all the RGB values as that is 
    // apparently what GE do for white (instead of 15)

    protocol_broadcast( 13, 13, 13, i );        
    delay( 50 );
  }  

  protocol_broadcast( 0, 0, 0, 0 );

  for ( int id = 0; id < 50; id++ ) {
    protocol_set_led_state( id, 15, 0, 0, MAX_BRIGHTNESS );
    delay( 50 );
  }

  delay( 1000 );
  protocol_broadcast( 0, 0, 0, 0 );
  
  // 2. Set alternating LEDs to red and blue and then reverse

  for ( byte mode = 0; mode < 2; ++mode ) {
    for ( int id = 0; id < 50; id++ ) {
      if ( id & 1 == mode ) {
        protocol_set_led_state( id, 15, 0, 0, MAX_BRIGHTNESS );
      }  else {
        protocol_set_led_state( id, 0, 0, 15, MAX_BRIGHTNESS );
      }
    }
    
    delay( 1000 );
  }

  protocol_broadcast( 0, 0, 0, 0 );
}
