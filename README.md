# Stranger Things Message Wall

Trying to hack a set of GE G-35 Color Effects lights and an ESP32 to implement a Message Wall, ala Stranger Things.

Status: rough, but working

protocol.cpp and protocol.h were cribbed from
[John Graham-Cumming's GECE](https://github.com/jgrahamc/gece/tree/master/gece),
and modified to work for the ESP32.

To setup:

* Copy secrets.h.tmpl to secrets.h and fill in the necessary values.
* Recompile
* Flash to an ESP32 board (I'm using a
[MakerFocus Board](https://www.makerfocus.com/products/makerfocus-esp32-oled-wifi-kit-esp-32-0-96-inch-oled-display-wifi-bluetooth-cp2102-internet-development-board-for-arduino-esp8266-nodemcu)

Wiring up the lights is a whole different game, I cribbed from
[deep darc](https://web.archive.org/web/20190322170653/http://www.deepdarc.com/2010/11/27/hacking-christmas-lights/)

* Cut the stock controller out from between the power supply and the
lights
* Wire up V+ and Ground from the power supply to the lights.
* Connect Ground from the lights to Ground from from the ESP32
  board.  (this was my first big mistake, I spent way more time than
  necessary trying to figure why things didn't work, and the whole
  time it was just ground differential)
* Connect pin out from the ESP32 (code uses pin2, but easy enough to
change) to the Data line on the lights.
* I have a
  [voltage shifter](https://www.amazon.com/gp/product/B07LG646VS/) in
  between the lights and the board, not sure if it's strictly
  necessary.
* Bazinga!
